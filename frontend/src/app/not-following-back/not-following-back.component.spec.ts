import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFollowingBackComponent } from './not-following-back.component';

describe('NotFollowingBackComponent', () => {
  let component: NotFollowingBackComponent;
  let fixture: ComponentFixture<NotFollowingBackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotFollowingBackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotFollowingBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
