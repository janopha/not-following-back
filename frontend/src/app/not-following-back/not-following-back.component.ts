import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

const base64 = require('base-64');

@Component({
  selector: 'app-not-following-back',
  templateUrl: './not-following-back.component.html',
  styleUrls: ['./not-following-back.component.css']
})
export class NotFollowingBackComponent {

  form: FormGroup;

  notFollowingBackUsernames: Array<string> = [];

  constructor(private formBuilder: FormBuilder,
              private http: HttpClient) {
    this.form = this.formBuilder.group({
      followers: ['', Validators.required],
      following: ['', Validators.required]
    });
  }

  get formControls(): any {
    return this.form.controls;
  }

  makeRequest() {
    const followers = this.formControls.followers?.value;
    const following = this.formControls.following?.value;
    this.backendRequest(base64.encode(unescape(encodeURIComponent(followers))),base64.encode(unescape(encodeURIComponent(following)))).then((response) => {
      response.not_following_back.forEach((item: string) => {
        this.notFollowingBackUsernames.push(item);
      });
      this.form.reset();
    }, (error) => {
      console.error(error);
      this.form.reset();
    });
  }

  backendRequest(followers: string, following: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put('/not-following-back', { 'followers': followers, 'following': following}).subscribe((response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      })
    });
  }

}
