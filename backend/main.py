import base64
from fastapi import FastAPI
from pydantic import BaseModel
from not_following_back import get_following_result

app = FastAPI()


@app.get("/")
async def root():
    return "App is running"


class NotFollowingBackRequest(BaseModel):
    followers: str
    following: str


@app.put("/not-following-back")
async def not_following_back(request: NotFollowingBackRequest):
    followers_html_decoded = base64.b64decode(request.followers)
    following_html_decoded = base64.b64decode(request.following)
    result = get_following_result(followers_html_decoded, following_html_decoded)
    return {"not_following_back": result.not_following_back,
            "number_of_accounts": result.number_of_accounts,
            "percent": result.percent}
