from bs4 import BeautifulSoup


class NotFollowingBackResponse:
    def __init__(self, not_following_back, number_of_accounts, percent):
        self.not_following_back = not_following_back
        self.number_of_accounts = number_of_accounts
        self.percent = percent


def read_html(html):
    parsed_html = BeautifulSoup(html)
    follow_entries = parsed_html.findAll('div', attrs={'class': '_ab8y'})
    follow_names = []
    for follow_entry in follow_entries:
        follow_name = follow_entry.text
        if not follow_name.__eq__('·'):
            follow_names.append(follow_name)
    return follow_names


def get_following_result(followers_html, following_html):
    followers = read_html(followers_html)
    following = read_html(following_html)
    not_following_back = []
    for follow_following in following:
        if follow_following in followers:
            continue
        if follow_following.__eq__("Verified") or follow_following.__eq__("Verifiziert"):
            continue
        not_following_back.append(follow_following)
    percent = "%.2f" % ((len(not_following_back) / len(following)) * 100)
    return NotFollowingBackResponse(not_following_back, len(not_following_back), float(percent))
